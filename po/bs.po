# translation of nautilus-vcs.HEAD.po to Bosnian
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER.
# Kemal Šanjta <gomez@lugzdk.ba>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: nautilus-vcs.HEAD\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2004-08-31 20:15+0200\n"
"PO-Revision-Date: 2004-09-01 03:26+0200\n"
"Last-Translator: Kemal Šanjta <gomez@lugzdk.ba>\n"
"Language-Team: Bosnian <lokal@linux.org.ba>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.3\n"

#: src/nautilus-cvs.c:199
#, c-format
msgid "%s (Modified)"
msgstr "%s (Izmjenjeno)"

#: src/nautilus-cvs.c:210
msgid "Not added"
msgstr "Nije dodano"

#: src/nautilus-cvs.c:448
msgid "Add to Repository"
msgstr "Dodaj u skladište"

#: src/nautilus-cvs.c:449
msgid "Add the selected files to the repository"
msgstr "Dodaj označene datoteke u skladište"

#: src/nautilus-cvs.c:462
msgid "Ignore"
msgstr "Ignoriši"

#: src/nautilus-cvs.c:463
msgid "Exclude the selected files from version control"
msgstr "Isključi označene datoteke iz kontole verzije"

#: src/nautilus-cvs.c:477
msgid "Commit"
msgstr "Izvrši"

#: src/nautilus-cvs.c:478
msgid "Commit changes to the selected files"
msgstr "Izvrši promjene označenim datotekama"

#: src/nautilus-cvs.c:492
msgid "Update"
msgstr "Nadogradi"

#: src/nautilus-cvs.c:493
msgid "Update selected files from the repository"
msgstr "Nagogradi označene datoteke iz skladišta"

#: src/nautilus-cvs.c:530
msgid "CVS Information"
msgstr "CVS Informacije"

#: src/nautilus-cvs.c:547
msgid "Name:"
msgstr "Ime:"

#: src/nautilus-cvs.c:554
msgid "Revision:"
msgstr "Ispravka:"

#: src/nautilus-cvs.c:561
msgid "Timestamp:"
msgstr "Vremenska oznaka:"

#: src/nautilus-cvs.c:568
msgid "Options:"
msgstr "Opcije:"

#: src/nautilus-cvs.c:575
msgid "Tag:"
msgstr "Etiketa:"

#: src/nautilus-cvs.c:583
msgid "Conflicts with CVS server."
msgstr "Konflikti sa CVS serverom."

#: src/nautilus-cvs.c:630
msgid "Version Control"
msgstr "Kontrola verzije"

#: src/nautilus-vcs.c:68
msgid "Version Info"
msgstr "Informacije verzije"

#: src/nautilus-vcs.c:69
msgid "Information from the version control system"
msgstr "Informacije iz kontrolnog sistema verzije"

