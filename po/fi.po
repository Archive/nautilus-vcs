# nautilus-vcs Finnish Translation
# Suomennos: http://gnome.fi/
# Copyright (C) 2004-2005 Free Software Foundation, Inc.
# Tommi Vainikainen <Tommi.Vainikainen@iki.fi>, 2004-2005.
#
# Sanastoa:
# repository (s) = keskusvarasto
# revision (s) = versio
# commit (v) = vie muutokset
# version control (s) = versionhallinta
# conflict (s) = ristiriita
#
msgid ""
msgstr ""
"Project-Id-Version: nautilus-vcs\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-07-19 13:52+0300\n"
"PO-Revision-Date: 2005-02-09 11:19+0200\n"
"Last-Translator: Tommi Vainikainen <Tommi.Vainikainen@iki.fi>\n"
"Language-Team: Finnish <gnome-fi-laatu@lists.sourceforge.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../src/nautilus-cvs.c:199
#, c-format
msgid "%s (Modified)"
msgstr "%s (Muokattu)"

#: ../src/nautilus-cvs.c:210
msgid "Not added"
msgstr "Ei lisätty"

#: ../src/nautilus-cvs.c:448
msgid "Add to Repository"
msgstr "Lisää keskusvarastoon"

#: ../src/nautilus-cvs.c:449
msgid "Add the selected files to the repository"
msgstr "Lisää valitut tiedostot keskusvarastoon"

#: ../src/nautilus-cvs.c:462
msgid "Ignore"
msgstr "Ohita"

#: ../src/nautilus-cvs.c:463
msgid "Exclude the selected files from version control"
msgstr "Jätä valitut tiedostot pois versionhallinnasta"

#: ../src/nautilus-cvs.c:477
msgid "Commit"
msgstr "Vie muutokset"

#: ../src/nautilus-cvs.c:478
msgid "Commit changes to the selected files"
msgstr "Vie valittujen tiedostojen muutokset"

#: ../src/nautilus-cvs.c:492
msgid "Update"
msgstr "Päivitä"

#: ../src/nautilus-cvs.c:493
msgid "Update selected files from the repository"
msgstr "Päivitä valitut tiedostot keskusvarastosta"

#: ../src/nautilus-cvs.c:530
msgid "CVS Information"
msgstr "CVS-tiedot"

#: ../src/nautilus-cvs.c:550
msgid "This file has not been added to CVS."
msgstr "Tätä tiedostoa ei ole lisätty CVS:ään."

#: ../src/nautilus-cvs.c:560
msgid "Name:"
msgstr "Nimi:"

#: ../src/nautilus-cvs.c:567
msgid "Revision:"
msgstr "Versio:"

#: ../src/nautilus-cvs.c:574
msgid "Timestamp:"
msgstr "Aikaleima:"

#: ../src/nautilus-cvs.c:581
msgid "Options:"
msgstr "Valinnat:"

#: ../src/nautilus-cvs.c:588
msgid "Tag:"
msgstr "Tagi:"

#: ../src/nautilus-cvs.c:596
msgid "Conflicts with CVS server."
msgstr "Ristiriitoja CVS-palvelimen kanssa."

#: ../src/nautilus-cvs.c:643
msgid "Version Control"
msgstr "Versionhallinta"

#: ../src/nautilus-vcs.c:68
msgid "Version Info"
msgstr "Versiotiedot"

#: ../src/nautilus-vcs.c:69
msgid "Information from the version control system"
msgstr "Tietoja versionhallintajärjestelmältä"
