/*
 *  nautilus-cvs-directory.h - Information about a directory stored in 
 *                             CVS.
 *
 *  Copyright (C) 2003 Novell, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dave Camp <dave@ximian.com>
 * 
 */

#include <config.h>
#include "nautilus-cvs-directory.h"

#include <eel/eel-vfs-extensions.h>

typedef struct {
	NautilusCvsDirectoryReadyCallback callback;
	gpointer user_data;
} ReadyCallback;

struct _NautilusCvsDirectoryDetails {
	char *uri;

	GList *pending_files;

	EelReadFileHandle *read_file_handle;

	GList *ready_callbacks;

	/* TRUE if all the relevent files have been read for this directory */
	gboolean directory_ready;

	/* The following attributes are available if directory_ready 
	 * is TRUE */
	GHashTable *entries;
	GList *ignores;
};


static void nautilus_cvs_directory_instance_init            (NautilusCvsDirectory               *cvs);
static void nautilus_cvs_directory_class_init               (NautilusCvsDirectoryClass          *class);

static GHashTable *cvs_directories = NULL;
static GObjectClass *parent_class;

static GList *default_ignores = NULL;

static void
free_entry (NautilusCvsEntry *entry)
{
	g_free (entry->name);
	g_free (entry->revision);
	g_free (entry->timestamp);
	g_free (entry->options);
	g_free (entry->tag);	
	g_free (entry);
}

static void
signal_directory_ready (NautilusCvsDirectory *directory)
{
	GList *l;

	directory->details->directory_ready = TRUE;

	/* The callbacks might unref the directory.  Make sure it 
	 * stays around until the function ends. */ 
	g_object_ref (directory);
	
	directory->details->ready_callbacks = 
		g_list_reverse (directory->details->ready_callbacks);
	
	for (l = directory->details->ready_callbacks; l != NULL; l = l->next) {
		ReadyCallback *ready_callback;
		
		ready_callback = l->data;

		(*ready_callback->callback) (directory,
					     ready_callback->user_data);

		g_free (ready_callback);
	}

	g_list_free (directory->details->ready_callbacks);
	directory->details->ready_callbacks = NULL;

	g_object_unref (directory);
}

gboolean 
nautilus_cvs_directory_is_ready (NautilusCvsDirectory *directory)
{
	g_return_val_if_fail (NAUTILUS_IS_CVS_DIRECTORY (directory), FALSE);

	return directory->details->directory_ready;
}

void     
nautilus_cvs_directory_call_when_ready (NautilusCvsDirectory              *directory,
					NautilusCvsDirectoryReadyCallback  callback,
					gpointer                           user_data)
{
	g_return_if_fail (NAUTILUS_IS_CVS_DIRECTORY (directory));

	if (directory->details->directory_ready) {
		(*callback) (directory, user_data);
	} else {
		ReadyCallback *ready_callback;

		ready_callback = g_new0 (ReadyCallback, 1);
		ready_callback->callback = callback;
		ready_callback->user_data = user_data;
		
		directory->details->ready_callbacks = 
			g_list_prepend (directory->details->ready_callbacks,
					ready_callback);
	}
}

gboolean                
nautilus_cvs_directory_has_entries (NautilusCvsDirectory *directory)
{
	g_return_val_if_fail (NAUTILUS_IS_CVS_DIRECTORY (directory), FALSE);
	g_return_val_if_fail (directory->details->directory_ready, FALSE);

	return (directory->details->entries != NULL);
}

const NautilusCvsEntry *
nautilus_cvs_directory_get_entry (NautilusCvsDirectory *directory,
				  const char *file_name)
{
	g_return_val_if_fail (NAUTILUS_IS_CVS_DIRECTORY (directory), NULL);
	g_return_val_if_fail (directory->details->directory_ready, NULL);
	
	if (directory->details->entries) {
		return g_hash_table_lookup (directory->details->entries, 
					    file_name);
	}

	return NULL;
}

gboolean                
nautilus_cvs_directory_is_ignored (NautilusCvsDirectory *directory,
				   const char *file_name)
{
	GList *l;
	
	g_return_val_if_fail (NAUTILUS_IS_CVS_DIRECTORY (directory), FALSE);
	g_return_val_if_fail (directory->details->directory_ready, FALSE);

	for (l = directory->details->ignores; l != NULL; l = l->next) {
		if (g_pattern_match_string (l->data, file_name)) {
			return TRUE;
		}
	}

	for (l = default_ignores; l != NULL; l = l->next) {
		if (g_pattern_match_string (l->data, file_name)) {
			return TRUE;
		}
	}

	return FALSE;
}

static GList *
get_lines (const char *file_contents, GnomeVFSFileSize file_size)
{
	GList *lines;
	int i;
	
	lines = NULL;
	i = 0;
	while (i < file_size) {
		int start;

		start = i;
		while (i < file_size && file_contents[i] != '\n') {
			i++;
		}

		if (i > start) {
			char *line;
		
			line = g_strndup (file_contents + start, i - start);
			lines = g_list_prepend (lines, line);
		}

		i++;
		
	}

	return g_list_reverse (lines);
}

static GList *
parse_ignore (const char *file_contents, GnomeVFSFileSize file_size)
{	
	GList *ignores;
	GList *lines;
	GList *l;

	ignores = NULL;
	lines = get_lines (file_contents, file_size);
	for (l = lines; l != NULL; l = l->next) {
		ignores = g_list_prepend (ignores,
					  g_pattern_spec_new (l->data));
		g_free (l->data);
	}
	g_list_free (lines);
	
	return ignores;
}

static void
ignore_read_callback (GnomeVFSResult result,
		      GnomeVFSFileSize file_size,
		      char *file_contents,
		      gpointer callback_data)
{
	NautilusCvsDirectory *directory;
	
	directory = NAUTILUS_CVS_DIRECTORY (callback_data);
	directory->details->read_file_handle = NULL;
	
	if (result == GNOME_VFS_OK) {	
		directory->details->ignores = 
			parse_ignore (file_contents, file_size);
		g_free (file_contents);
	}

	/* Couldn't read .cvsignore, we're done */
	signal_directory_ready (directory);
}

static void
read_ignore (NautilusCvsDirectory *directory)
{
	char *ignore_uri;
	
	ignore_uri = g_strdup_printf ("%s/.cvsignore", 
				      directory->details->uri);

	directory->details->read_file_handle = eel_read_entire_file_async
		(ignore_uri,
		 GNOME_VFS_PRIORITY_DEFAULT,
		 ignore_read_callback,
		 directory);

	g_free (ignore_uri);
}

static NautilusCvsEntry *
parse_line (const char *string)
{	
	if (string[0] == '/') {
		NautilusCvsEntry *entry;
		char **fields;
		int count;
		

		g_print ("string: %s\n", string);
		
		fields = g_strsplit (string, "/", 0);

		count = 0;
		while (fields[count]) {
			count++;
		}
		
		if (count == 6) {
			entry = g_new0 (NautilusCvsEntry, 1);
			entry->is_directory = FALSE;
			entry->name = g_strdup (fields[1]);
			entry->revision = g_strdup (fields[2]);
			g_print ("revision is %s\n", entry->revision);
			entry->timestamp = g_strdup (fields[3]);
			entry->options = g_strdup (fields[4]);
			entry->tag = g_strdup (fields[5]);
		} else {
			entry = NULL;
		}

		g_strfreev (fields);
		return entry;
	} else if (string[0] == 'D') {
		char **fields;		

		fields = g_strsplit (string, "/", 0);
		if (fields[1] != '\0') {
			NautilusCvsEntry *entry;

			entry = g_new0 (NautilusCvsEntry, 1);
			entry->is_directory = TRUE;
			entry->name = g_strdup (fields[1]);
			
			return entry;
		} else {
			return NULL;
		}
	} else {
		return NULL;
	}
}

static void
apply_entries_log (const char *file_contents,
		   GnomeVFSFileSize file_size,
		   GHashTable *entries)
{
	GList *lines;
	GList *l;
	NautilusCvsEntry *entry;

	lines = get_lines (file_contents, file_size);
	
	for (l = lines; l != NULL; l = l->next) {
		char *line = l->data;
		
		if (line[0] == 'A' && line[1] == ' ') {
			entry = parse_line (line + 2);
			
			if (entry) {
				g_hash_table_insert (entries, 
						     entry->name, 
						     entry);
			}
		} else if (line[0] == 'R' && line[1] == ' ') {
			entry = parse_line (line + 2);
			
			if (entry) {
				g_hash_table_remove (entries, entry->name);
			}
		}			

		g_free (l->data);
	}

	g_list_free (lines);
}

static void
entries_log_read_callback (GnomeVFSResult result,
			   GnomeVFSFileSize file_size,
			   char *file_contents,
			   gpointer callback_data)
{
	NautilusCvsDirectory *directory;
	
	directory = NAUTILUS_CVS_DIRECTORY (callback_data);
	directory->details->read_file_handle = NULL;
	
	if (result == GNOME_VFS_OK) {	
		apply_entries_log (file_contents,
				   file_size,
				   directory->details->entries);
		g_free (file_contents);
	}
	
	/* Now read the .cvsignore file */
	read_ignore (directory);
}

static void
read_entries_log (NautilusCvsDirectory *directory)
{
	char *entries_uri;
	
	entries_uri = g_strdup_printf ("%s/CVS/Entries.Log", 
				       directory->details->uri);

	directory->details->read_file_handle = eel_read_entire_file_async
		(entries_uri,
		 GNOME_VFS_PRIORITY_DEFAULT,
		 entries_log_read_callback,
		 directory);

	g_free (entries_uri);
}

static GHashTable *
parse_entries (const char *file_contents,
	       GnomeVFSFileSize file_size)
{
	GHashTable *entries;
	GList *lines;
	GList *l;
	NautilusCvsEntry *entry;

	entries = g_hash_table_new_full (g_str_hash,
					 g_str_equal,
					 NULL,
					 (GDestroyNotify)free_entry);
	
	lines = get_lines (file_contents, file_size);
	
	for (l = lines; l != NULL; l = l->next) {
		entry = parse_line (l->data);
		g_free (l->data);

		if (entry) {
			g_hash_table_insert (entries, 
					     entry->name, 
					     entry);
		}
	}

	g_list_free (lines);

	return entries;
}

static void
entries_read_callback (GnomeVFSResult result,
		       GnomeVFSFileSize file_size,
		       char *file_contents,
		       gpointer callback_data)
{
	NautilusCvsDirectory *directory;
	
	directory = NAUTILUS_CVS_DIRECTORY (callback_data);
	directory->details->read_file_handle = NULL;
	
	if (result == GNOME_VFS_OK) {	
		directory->details->entries = parse_entries (file_contents,
							     file_size);
		g_free (file_contents);
		
                /* Now read the CVS/Entries.Log file */
		read_entries_log (directory);
	} else {
		/* Couldn't read CVS/Entries, we're done */
		signal_directory_ready (directory);
	}
}

static void
read_entries (NautilusCvsDirectory *directory)
{
	char *entries_uri;
	
	entries_uri = g_strdup_printf ("%s/CVS/Entries", 
				       directory->details->uri);

	directory->details->read_file_handle = eel_read_entire_file_async
		(entries_uri,
		 GNOME_VFS_PRIORITY_DEFAULT,
		 entries_read_callback,
		 directory);

	g_free (entries_uri);
}

static NautilusCvsDirectory *
nautilus_cvs_directory_new (const char *uri)
{
	NautilusCvsDirectory *directory;

	directory = g_object_new (NAUTILUS_TYPE_CVS_DIRECTORY, NULL);
	directory->details->uri = g_strdup (uri);

	g_hash_table_insert (cvs_directories, g_strdup (uri), directory);

	return directory;
}

NautilusCvsDirectory * 
nautilus_cvs_directory_get (const char *uri)
{
	NautilusCvsDirectory *directory;
	char *canonical_uri;

	if (!cvs_directories) {
		cvs_directories = g_hash_table_new_full (g_str_hash,
							 g_str_equal,
							 g_free,
							 NULL);
	}

	canonical_uri = eel_make_uri_canonical (uri);

	/* Find an existing NautilusCvsDirectory if possible, and create
	 * a new one if necessary. */
	directory = g_hash_table_lookup (cvs_directories, canonical_uri);
	if (directory) {
		g_object_ref (directory);
	} else {
		directory = nautilus_cvs_directory_new (canonical_uri);
		read_entries (directory);
	}

	g_free (canonical_uri);

	return directory;
}

static void
nautilus_cvs_directory_dispose (GObject *obj)
{
	NautilusCvsDirectory *directory;

	directory = NAUTILUS_CVS_DIRECTORY (obj);
	
	if (directory->details->read_file_handle) {
		eel_read_file_cancel (directory->details->read_file_handle);
	}

	if (g_hash_table_lookup (cvs_directories, 
				 directory->details->uri) != NULL) {
		g_hash_table_remove (cvs_directories, directory->details->uri);
	}

	G_OBJECT_CLASS (parent_class)->dispose (obj);
}

static void
nautilus_cvs_directory_finalize (GObject *obj)
{
	NautilusCvsDirectory *directory;
	GList *l;
	
	directory = NAUTILUS_CVS_DIRECTORY (obj);

	g_free (directory->details->uri);
	
	if (directory->details->entries) {
		g_hash_table_destroy (directory->details->entries);
	}

	for (l = directory->details->ignores; l != NULL; l = l->next) {
		g_pattern_spec_free (l->data);
	}
	g_list_free (directory->details->ignores);

	g_free (directory->details);

	G_OBJECT_CLASS (parent_class)->finalize (obj);
}

static void 
nautilus_cvs_directory_instance_init (NautilusCvsDirectory *directory)
{
	directory->details = g_new0 (NautilusCvsDirectoryDetails, 1);
}

static void
nautilus_cvs_directory_class_init (NautilusCvsDirectoryClass *class)
{
	int i;
	
	static char *default_ignore_strings[] = {
		"RCS", "SCCS", "CVS","CVS.adm", "RCSLOG", "cvslog.*",
		"tags", "TAGS", ".make.state", ".nse_depinfo", "*~",
		"#*", ".#*", ",*", "_$*", "*$", "*.old", "*.bak",
		"*.BAK", "*.orig", "*.rej", ".del-*", "*.a", "*.olb", 
		"*.o", "*.obj", "*.so", "*.exe", "*.Z", "*.elc",
		"*.ln", "core", NULL,
	};
	
	for (i = 0; default_ignore_strings[i] != NULL; i++) {
		GPatternSpec *spec;
		
		spec = g_pattern_spec_new (default_ignore_strings[i]);
		
		default_ignores = g_list_prepend (default_ignores, spec);
	}
	
	parent_class = g_type_class_peek_parent (class);	

	G_OBJECT_CLASS (class)->dispose = nautilus_cvs_directory_dispose;
	G_OBJECT_CLASS (class)->finalize = nautilus_cvs_directory_finalize;
}

/* Type registration.  Because this type is implemented in a module
 * that can be unloaded, we separate type registration from get_type().
 * the type_register() function will be called by the module's
 * initialization function. */
static GType cvs_directory_type = 0;

GType
nautilus_cvs_directory_get_type (void) 
{
	return cvs_directory_type;
}

void
nautilus_cvs_directory_register_type (GTypeModule *module)
{
	static const GTypeInfo info = {
		sizeof (NautilusCvsDirectoryClass),
		(GBaseInitFunc) NULL,
		(GBaseFinalizeFunc) NULL,
		(GClassInitFunc) nautilus_cvs_directory_class_init,
		NULL, 
		NULL,
		sizeof (NautilusCvsDirectory),
		0,
		(GInstanceInitFunc) nautilus_cvs_directory_instance_init,
	};
	
	cvs_directory_type = 
		g_type_module_register_type (module,
					     G_TYPE_OBJECT,
					     "NautilusCvsDirectory",
					     &info, 0);
}
