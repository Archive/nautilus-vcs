/*
 *  nautilus-cvs-directory.h - Information about a directory stored in 
 *                             CVS.
 * 
 *  Copyright (C) 2003 Novell, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dave Camp <dave@ximian.com>
 * 
 */

#ifndef NAUTILUS_CVS_DIRECTORY_H
#define NAUTILUS_CVS_DIRECTORY_H

#include <glib-object.h>

G_BEGIN_DECLS

#define NAUTILUS_TYPE_CVS_DIRECTORY  (nautilus_cvs_directory_get_type ())
#define NAUTILUS_CVS_DIRECTORY(o)    (G_TYPE_CHECK_INSTANCE_CAST ((o), NAUTILUS_TYPE_CVS_DIRECTORY, NautilusCvsDirectory))
#define NAUTILUS_IS_CVS_DIRECTORY(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), NAUTILUS_TYPE_CVS_DIRECTORY))
typedef struct _NautilusCvsDirectory        NautilusCvsDirectory;
typedef struct _NautilusCvsDirectoryDetails NautilusCvsDirectoryDetails;
typedef struct _NautilusCvsDirectoryClass   NautilusCvsDirectoryClass;
typedef struct _NautilusCvsEntry            NautilusCvsEntry;

typedef void (*NautilusCvsDirectoryReadyCallback) (NautilusCvsDirectory *directory,
						   gpointer user_data);

struct _NautilusCvsDirectory {
	GObject parent_slot;

	NautilusCvsDirectoryDetails *details;
};

struct _NautilusCvsDirectoryClass {
	GObjectClass parent_slot;

	/* No extra class members */
};

struct _NautilusCvsEntry {
	char *name;
	gboolean is_directory;
	
	/* Only valid if the entry is not a directory */
	char *revision;
	char *timestamp;
	char *options;
	char *tag;
	
	gboolean conflict;
};

GType                   nautilus_cvs_directory_get_type        (void);
void                    nautilus_cvs_directory_register_type   (GTypeModule                       *module);
NautilusCvsDirectory *  nautilus_cvs_directory_get             (const char                        *directory_uri);
gboolean                nautilus_cvs_directory_is_ready        (NautilusCvsDirectory              *directory);
void                    nautilus_cvs_directory_call_when_ready (NautilusCvsDirectory              *directory,
								NautilusCvsDirectoryReadyCallback  callback,
								gpointer                           user_data);

gboolean                nautilus_cvs_directory_has_entries     (NautilusCvsDirectory              *directory);
const NautilusCvsEntry *nautilus_cvs_directory_get_entry       (NautilusCvsDirectory              *directory,
								const char                        *file_name);
gboolean                nautilus_cvs_directory_is_ignored      (NautilusCvsDirectory              *directory,
								const char                        *file_name);
G_END_DECLS

#endif
