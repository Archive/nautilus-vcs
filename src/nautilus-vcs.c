/*
 *  nautilus-vcs.c - Version control extension for Nautilus
 * 
 *  Copyright (C) 2003 Novell, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dave Camp <dave@ximian.com>
 * 
 */

#include <config.h>

#include <libnautilus-extension/nautilus-extension-types.h>
#include <libnautilus-extension/nautilus-column-provider.h>

#include "nautilus-cvs.h"
#include "nautilus-cvs-directory.h"

#include <glib/gi18n-lib.h>

/* NautilusVcs provider.  This object handles things that are shared
 * across different version control systems. */

#define NAUTILUS_TYPE_VCS  (nautilus_vcs_get_type ())
#define NAUTILUS_VCS(o)    (G_TYPE_CHECK_INSTANCE_CAST ((o), NAUTILUS_TYPE_VCS, NautilusVcs))

typedef struct {
	GObject parent_slot;
} NautilusVcs;

typedef struct {
	GObjectClass parent_slot;
} NautilusVcsClass;

static GType nautilus_vcs_get_type      (void);
static void  nautilus_vcs_register_type (GTypeModule *module);

static GObjectClass *parent_class;

/* nautilus_column_provider_get_columns
 *  
 * This function is called by Nautilus to get the column objects
 * exported by the provider.
 *
 * The function should return a GList of allocated NautilusColumn
 * items.
 */
static GList *
nautilus_vcs_get_columns (NautilusColumnProvider *provider)
{
	NautilusColumn *column;
	
	column = nautilus_column_new ("NautilusVcs::version_info_column",
				      "NautilusVcs::version_info",
				      _("Version Info"),
				      _("Information from the version control system"));
	
	return g_list_append (NULL, column);
}

static void
nautilus_vcs_column_provider_iface_init (NautilusColumnProviderIface *iface)
{
	iface->get_columns = nautilus_vcs_get_columns;
}

static void
nautilus_vcs_instance_init (NautilusVcs *vcs)
{
}

static void
nautilus_vcs_class_init (NautilusVcsClass *class)
{
        parent_class = g_type_class_peek_parent (class);
}

static GType vcs_type = 0;

static GType
nautilus_vcs_get_type (void)
{
	return vcs_type;
}

static void
nautilus_vcs_register_type (GTypeModule *module)
{
	static const GTypeInfo info = {
                sizeof (NautilusVcsClass),
                (GBaseInitFunc) NULL,
                (GBaseFinalizeFunc) NULL,
                (GClassInitFunc) nautilus_vcs_class_init,
                NULL,
                NULL,
                sizeof (NautilusVcs),
                0,
                (GInstanceInitFunc) nautilus_vcs_instance_init,
        };

	static const GInterfaceInfo column_provider_iface_info = {
		(GInterfaceInitFunc)nautilus_vcs_column_provider_iface_init,
		NULL,
		NULL
	};
	
	vcs_type = g_type_module_register_type (module, 
						G_TYPE_OBJECT,
						"NautilusVcs",
						&info, 0);
	g_type_module_add_interface (module,
				     vcs_type,
				     NAUTILUS_TYPE_COLUMN_PROVIDER,
				     &column_provider_iface_info);
}

/* Extension module functions.  These functions are defined in 
 * nautilus-extensions-types.h, and must be implemented by all 
 * extensions. */

/* Initialization function.  In addition to any module-specific 
 * initialization, any types implemented by the module should 
 * be registered here. */
void
nautilus_module_initialize (GTypeModule  *module)
{
	g_print ("Initializing nautilus-vcs extension\n");

	nautilus_vcs_register_type (module);
	nautilus_cvs_register_type (module);
	nautilus_cvs_directory_register_type (module);
}

/* Perform module-specific shutdown. */
void
nautilus_module_shutdown   (void)
{
	g_print ("Shutting down nautilus-vcs extension\n");
}

/* List all the extension types.  */
void 
nautilus_module_list_types (const GType **types,
			    int          *num_types)
{
	static GType type_list[2];
	
	type_list[0] = NAUTILUS_TYPE_VCS;
	type_list[1] = NAUTILUS_TYPE_CVS;

	*types = type_list;
	*num_types = 2;
}



