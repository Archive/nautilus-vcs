/*
 *  nautilus-cvs.c - CVS file info
 * 
 *  Copyright (C) 2003 Novell, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dave Camp <dave@ximian.com>
 * 
 */

#include <config.h>
#include "nautilus-cvs.h"

/* Nautilus extension headers */
#include <libnautilus-extension/nautilus-extension-types.h>
#include <libnautilus-extension/nautilus-file-info.h>
#include <libnautilus-extension/nautilus-info-provider.h>
#include <libnautilus-extension/nautilus-menu-provider.h>
#include <libnautilus-extension/nautilus-property-page-provider.h>

/* Other headers */
#include "nautilus-cvs-directory.h"

#include <eel/eel-vfs-extensions.h>
#include <glib/gi18n-lib.h>
#include <gtk/gtktable.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtklabel.h>
#include <string.h>
#include <time.h>

typedef struct {
	gboolean cancelled;
	NautilusInfoProvider *provider;
	NautilusFileInfo *file;
	GClosure *update_complete;
} NautilusCvsHandle;

static void nautilus_cvs_instance_init            (NautilusCvs               *cvs);
static void nautilus_cvs_class_init               (NautilusCvsClass          *class);

static GObjectClass *parent_class;

static GHashTable *
get_interested_files (NautilusCvsDirectory *directory)
{
	GHashTable *interested_files;
	
	interested_files = g_object_get_data (G_OBJECT (directory),
					      "interested_files");
	if (!interested_files) {
		interested_files = g_hash_table_new (g_direct_hash,
						     g_direct_equal);
		g_object_set_data_full (G_OBJECT (directory),
					"interested_files",
					interested_files,
					(GDestroyNotify)g_hash_table_destroy);
	}

	return interested_files;
}

static void
file_info_weak_notify (NautilusCvsDirectory *directory,
		       NautilusFileInfo *file)
{
	GHashTable *interested_files;
	
	interested_files = get_interested_files (directory);

	g_hash_table_remove (interested_files, file);

	g_object_unref (directory);	
}

/* Keep the directory info around for at least as long as the 
 * NautilusFileInfo is around. */
static void
watch_directory (NautilusCvsDirectory *directory,
		 NautilusFileInfo *file)
{
	GHashTable *interested_files;
	
	interested_files = get_interested_files (directory);
	
	/* Keep one ref per interested file  */
	if (!g_hash_table_lookup (interested_files, file)) {
		g_object_ref (directory);

		g_hash_table_insert (interested_files, file, (gpointer)1);

		/* Arrange to unref when the object goes away */
		g_object_weak_ref (G_OBJECT (file), 
				   (GWeakNotify)file_info_weak_notify,
				   directory);
	}
}

static gboolean
file_is_modified (NautilusFileInfo *file,
		  NautilusCvsDirectory *directory)
{
	gboolean ret;
	const NautilusCvsEntry *entry;
	char *name;
	
	name = nautilus_file_info_get_name (file);
	entry = nautilus_cvs_directory_get_entry (directory, name);
	g_free (name);
	
	ret = FALSE;

	if (entry && !entry->is_directory) {
		struct tm mtime;
		const GnomeVFSFileInfo *info;
		char timestamp[512];
		info = nautilus_file_info_get_vfs_file_info (file);
		
		gmtime_r (&info->mtime, &mtime);
		if (strftime (timestamp, 512, 
			      "%a %b %e %H:%M:%S %Y", 
			      &mtime) != 0) {
			if (strcmp (timestamp, entry->timestamp) != 0) {
				ret = TRUE;
			}
		}
	}

	return ret;
}

static gboolean
file_is_orphaned (NautilusFileInfo *file,
		  NautilusCvsDirectory *directory)
{
	gboolean ret;
	const NautilusCvsEntry *entry;
	char *name;
	
	name = nautilus_file_info_get_name (file);
	entry = nautilus_cvs_directory_get_entry (directory, name);
	
	ret = FALSE;

	if (!entry) {
		if (!nautilus_cvs_directory_is_ignored (directory, name)) {
			ret = TRUE;
		}
	}

	g_free (name);

	return ret;
}

static gboolean
file_is_in_cvs (NautilusFileInfo *file,
		NautilusCvsDirectory *directory)
{
	char *name;
	const NautilusCvsEntry *entry;
	
	name = nautilus_file_info_get_name (file);
	entry = nautilus_cvs_directory_get_entry (directory, name);
	g_free (name);

	return (entry != NULL);
}

static char*
get_version_info (NautilusFileInfo *file,
		  NautilusCvsDirectory *directory)
{
	char *ret;
	char *name;
	const NautilusCvsEntry *entry;
	
	name = nautilus_file_info_get_name (file);
	entry = nautilus_cvs_directory_get_entry (directory, name);
	
	ret = NULL;
	
	if (entry && !entry->is_directory) {
		if (file_is_modified (file, directory)) {
			ret = g_strdup_printf (_("%s (Modified)"),
						entry->revision);
		} else {
			ret = g_strdup (entry->revision);
		}		
	} else if (entry && !entry->is_directory) {
		ret = g_strdup ("Version controlled");
	} else {
		if (nautilus_cvs_directory_is_ignored (directory, name)) {
			ret = g_strdup ("--");
		} else {
			ret = g_strdup (_("Not added"));
		}
	}

	g_free (name);

	return ret;
}

static NautilusCvsDirectory *
get_cvs_directory (NautilusFileInfo *file)
{
	NautilusCvsDirectory *directory;
	char *directory_uri;
	
	directory_uri = nautilus_file_info_get_parent_uri (file);
	directory = nautilus_cvs_directory_get (directory_uri);
	g_free (directory_uri);
	
	return directory;
}

static void
update_file_info (NautilusFileInfo *file,
		  NautilusCvsDirectory *directory)
{
	g_return_if_fail (nautilus_cvs_directory_is_ready (directory));

	if (nautilus_cvs_directory_has_entries (directory)) {
		char *version_info;
		
		if (file_is_modified (file, directory)) {
			nautilus_file_info_add_emblem (file, "cvs-modified");				
		} else if (file_is_orphaned (file, directory)) {
			nautilus_file_info_add_emblem (file, "cvs-new-file");
		}
		
		version_info = get_version_info (file, directory);
		if (version_info) {
			nautilus_file_info_add_string_attribute
				(file, "NautilusVcs::version_info", 
				 version_info);
			g_free (version_info);
		}
	}
}

static void
directory_ready_callback (NautilusCvsDirectory *directory,
			  gpointer user_data)
{
	NautilusCvsHandle *handle;

	handle = user_data;
	
	if (!handle->cancelled) {
		update_file_info (handle->file, directory);
		
		nautilus_info_provider_update_complete_invoke 
			(handle->update_complete,
			 handle->provider,
			 (NautilusOperationHandle*)handle,
			 NAUTILUS_OPERATION_COMPLETE);
	}

	/* We're done with the closure, get rid of it */
	g_closure_unref (handle->update_complete);
	g_object_unref (handle->file);
	g_free (handle);
}

/* Implementation of the NautilusInfoProvider interface */

/* nautilus_info_provider_update_file_info 
 * This function is called by Nautilus when it wants the extension to 
 * fill in data about the file.  It passes a NautilusFileInfo object,
 * which the extension can use to read data from the file, and which
 * the extension should add data to.
 *
 * If the data can be added immediately (without doing blocking IO), 
 * the extension can do so, and return NAUTILUS_OPERATION_COMPLETE.  
 * In this case the 'update_complete' and 'handle' parameters can be 
 * ignored.
 * 
 * If waiting for the deata would block the UI, the extension should
 * perform the task asynchronously, and return 
 * NAUTILUS_OPERATION_IN_PROGRESS.  The function must also set the 
 * 'handle' pointer to a value unique to the object, and invoke the
 * 'update_complete' closure when the update is done.
 * 
 * If the extension encounters an error, it should return 
 * NAUTILUS_OPERATION_FAILED.
 */
static NautilusOperationResult
nautilus_cvs_update_file_info (NautilusInfoProvider *provider,
			       NautilusFileInfo *file,
			       GClosure *update_complete,
			       NautilusOperationHandle **handle)
{
	NautilusCvsDirectory *directory;
	NautilusOperationResult result;
	
	directory = get_cvs_directory (file);

	/* Keep the directory alive for as long as the file is around */
	watch_directory (directory, file);

	if (nautilus_cvs_directory_is_ready (directory)) {
		update_file_info (file, directory);
		
		result = NAUTILUS_OPERATION_COMPLETE;
	} else {
		NautilusCvsHandle *cvs_handle;
		
		cvs_handle = g_new0 (NautilusCvsHandle, 1);

		cvs_handle->provider = provider;
		cvs_handle->file = g_object_ref (file);

		/* We need to keep the closure around until we call it
		 * back. */
		cvs_handle->update_complete = g_closure_ref (update_complete);

		nautilus_cvs_directory_call_when_ready 
			(directory, directory_ready_callback, cvs_handle);

		*handle = (NautilusOperationHandle*)cvs_handle;

		result = NAUTILUS_OPERATION_IN_PROGRESS;
	}

	g_object_unref (directory);

	return result;
}

static void
nautilus_cvs_cancel_update (NautilusInfoProvider *provider,
			    NautilusOperationHandle *handle)
{
	NautilusCvsHandle *cvs_handle;
	
	cvs_handle = (NautilusCvsHandle*)handle;
	
	cvs_handle->cancelled = TRUE;
}

static void 
nautilus_cvs_info_provider_iface_init (NautilusInfoProviderIface *iface)
{
	iface->update_file_info = nautilus_cvs_update_file_info;
	iface->cancel_update = nautilus_cvs_cancel_update;
}

/* Implementation of the NautilusMenuProvider interface */

static void
update_callback (NautilusMenuItem *item,
		 gpointer user_data)
{
	g_print ("Update selected\n");	
}

static void
commit_callback (NautilusMenuItem *item,
		 gpointer user_data)
{
	g_print ("Commit selected\n");	
}

static void
add_callback (NautilusMenuItem *item,
	      gpointer user_data)
{
	g_print ("Add selected\n");	
}	 

static void
ignore_callback (NautilusMenuItem *item,
		 gpointer user_data)
{
	g_print ("Ignore selected\n");	
}

/* nautilus_menu_provider_get_file_items
 *  
 * This function is called by Nautilus when it wants context menu
 * items from the extension.
 *
 * This function is called in the main thread before a context menu
 * is shown, so it should return quickly.
 * 
 * The function should return a GList of allocated NautilusMenuItem
 * items.
 */
static GList *
nautilus_cvs_get_file_items (NautilusMenuProvider *provider,
			     GtkWidget *window,
			     GList *files)
{
	NautilusMenuItem *item;
	GList *l;
	GList *items;
	gboolean can_update;
	gboolean has_modified;
	gboolean has_orphaned;
	
	can_update = FALSE;
	has_modified = FALSE;
	has_orphaned = FALSE;

	for (l = files; l != NULL; l = l->next) {
		NautilusCvsDirectory *directory;
		NautilusFileInfo *file;
		
		file = NAUTILUS_FILE_INFO (l->data);
		directory = get_cvs_directory (file);
		
		if (nautilus_cvs_directory_is_ready (directory) &&
		    nautilus_cvs_directory_has_entries (directory)) {
			if (file_is_modified (file, directory)) {
				has_modified = TRUE;
			}
			if (file_is_orphaned (file, directory)) {
				has_orphaned = TRUE;
			}
			if (file_is_in_cvs (file, directory)) {
				can_update = TRUE;
			}	
		}

		g_object_unref (directory);
	}

	items = NULL;
	
	if (has_orphaned) {		
		item = nautilus_menu_item_new ("NautilusCvs::add",
					       _("Add to Repository"),
					       _("Add the selected files to the repository"),
					       NULL);
		g_signal_connect (item, "activate",
				  G_CALLBACK (add_callback),
				  provider);
		g_object_set_data (G_OBJECT (item), 
				   "files",
				   nautilus_file_info_list_copy (files));
		
		items = g_list_append (items, item);


		item = nautilus_menu_item_new ("NautilusCvs::ignore",
					       _("Ignore"),
					       _("Exclude the selected files from version control"),
					       NULL);
		g_signal_connect (item, "activate",
				  G_CALLBACK (ignore_callback),
				  provider);
		g_object_set_data (G_OBJECT (item), 
				   "files",
				   nautilus_file_info_list_copy (files));
		
		items = g_list_append (items, item);
	}

	if (has_modified) {
		item = nautilus_menu_item_new ("NautilusCvs::commit",
					       _("Commit"),
					       _("Commit changes to the selected files"),
					       NULL);
		g_signal_connect (item, "activate",
				  G_CALLBACK (commit_callback),
				  provider);
		g_object_set_data (G_OBJECT (item), 
				   "files",
				   nautilus_file_info_list_copy (files));
		
		items = g_list_append (items, item);
	}

	if (can_update) {
		item = nautilus_menu_item_new ("NautilusCvs::update",
					       _("Update"),
					       _("Update selected files from the repository"),
					       NULL);
		g_signal_connect (item, "activate",
				  G_CALLBACK (update_callback),
				  provider);
		g_object_set_data (G_OBJECT (item), 
				   "files",
				   nautilus_file_info_list_copy (files));
		
		items = g_list_append (items, item);
	}

	return items;
}

static void 
nautilus_cvs_menu_provider_iface_init (NautilusMenuProviderIface *iface)
{
	iface->get_file_items = nautilus_cvs_get_file_items;
}

static GtkWidget *
create_property_page (NautilusFileInfo *file, NautilusCvsDirectory *directory)
{
	GtkWidget *page, *vbox, *hbox, *table;
	GtkWidget *label;
	gchar *str;
	const NautilusCvsEntry *entry;

	entry = nautilus_cvs_directory_get_entry (directory, nautilus_file_info_get_name (file));

	page = gtk_vbox_new (FALSE, 18);
	gtk_container_set_border_width (GTK_CONTAINER (page), 12);

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_add (GTK_CONTAINER (page), vbox);

	str = g_strdup_printf ("<b>%s</b>", _("CVS Information"));
	label = g_object_new (GTK_TYPE_LABEL, "label", str, "xalign", 0.0, "use-markup", TRUE, NULL);
	g_free (str);
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (vbox), hbox);

	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	table = g_object_new (GTK_TYPE_TABLE, "n-rows", 5, "n-columns", 2, "row-spacing", 6,
			      "column-spacing", 12, NULL);
	gtk_container_add (GTK_CONTAINER (hbox), table);

	if (entry == NULL) {
		label = g_object_new (GTK_TYPE_LABEL, 
				      "label", 
				      _("This file has not been added to CVS."),
				      "xalign", 0.0,
				      NULL);
		gtk_table_attach (GTK_TABLE (table), label, 0, 2, 0, 1, GTK_FILL, 0, 0, 0);
		gtk_widget_show_all (page);
		
		return page;
	}
				      

	label = g_object_new (GTK_TYPE_LABEL, "label", _("Name:"), "xalign", 0.0, NULL);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1, GTK_FILL, 0, 0, 0);

	label = g_object_new (GTK_TYPE_LABEL, "label", entry->name, "xalign", 0.0,
			      "selectable", TRUE, NULL);
	gtk_table_attach (GTK_TABLE (table), label, 1, 2, 0, 1, (GTK_FILL | GTK_EXPAND), 0, 0, 0);

	label = g_object_new (GTK_TYPE_LABEL, "label", _("Revision:"), "xalign", 0.0, NULL);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2, GTK_FILL, 0, 0, 0);

	label = g_object_new (GTK_TYPE_LABEL, "label", entry->revision, "xalign", 0.0,
			      "selectable", TRUE, NULL);
	gtk_table_attach (GTK_TABLE (table), label, 1, 2, 1, 2, (GTK_FILL | GTK_EXPAND), 0, 0, 0);

	label = g_object_new (GTK_TYPE_LABEL, "label", _("Timestamp:"), "xalign", 0.0, NULL);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 2, 3, GTK_FILL, 0, 0, 0);

	label = g_object_new (GTK_TYPE_LABEL, "label", entry->timestamp, "xalign", 0.0,
			      "selectable", TRUE, NULL);
	gtk_table_attach (GTK_TABLE (table), label, 1, 2, 2, 3, (GTK_FILL | GTK_EXPAND), 0, 0, 0);

	label = g_object_new (GTK_TYPE_LABEL, "label", _("Options:"), "xalign", 0.0, NULL);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 3, 4, GTK_FILL, 0, 0, 0);

	label = g_object_new (GTK_TYPE_LABEL, "label", entry->options, "xalign", 0.0,
			      "selectable", TRUE, NULL);
	gtk_table_attach (GTK_TABLE (table), label, 1, 2, 3, 4, (GTK_FILL | GTK_EXPAND), 0, 0, 0);

	label = g_object_new (GTK_TYPE_LABEL, "label", _("Tag:"), "xalign", 0.0, NULL);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 4, 5, GTK_FILL, 0, 0, 0);

	label = g_object_new (GTK_TYPE_LABEL, "label", entry->tag, "xalign", 0.0,
			      "selectable", TRUE, NULL);
	gtk_table_attach (GTK_TABLE (table), label, 1, 2, 4, 5, (GTK_FILL | GTK_EXPAND), 0, 0, 0);

	if (entry->conflict) {
		str = g_strdup_printf ("<i>%s</i>", _("Conflicts with CVS server."));
		label = g_object_new (GTK_TYPE_LABEL, "label", str, "xalign", 0.0,
				      "use-markup", TRUE, NULL);
		g_free (str);
		gtk_table_attach (GTK_TABLE (table), label, 0, 2, 5, 6,
				  (GTK_FILL | GTK_EXPAND), 0, 0, 0);
	}

	gtk_widget_show_all (page);
	
	return page;
}

/* nautilus_property_page_provider_get_pages
 *  
 * This function is called by Nautilus when it wants property page
 * items from the extension.
 *
 * This function is called in the main thread before a property page
 * is shown, so it should return quickly.
 * 
 * The function should return a GList of allocated NautilusPropertyPage
 * items.
 */
static GList *
nautilus_cvs_get_property_pages (NautilusPropertyPageProvider *provider,
				 GList *files)
{
	GList *pages;
	NautilusPropertyPage *page;
	NautilusCvsDirectory *directory;
	NautilusFileInfo *file;
	
	/* Only show the property page if 1 file is selected */
	if (!files || files->next != NULL) {
		return NULL;
	}

	pages = NULL;
	
	file = NAUTILUS_FILE_INFO (files->data);
	directory = get_cvs_directory (file);
	
	if (nautilus_cvs_directory_is_ready (directory) &&
	    nautilus_cvs_directory_has_entries (directory)) {
		page = nautilus_property_page_new
			("NautilusCvs::property_page", 
			 gtk_label_new (_("Version Control")),
			 create_property_page (file, directory));
		pages = g_list_append (pages, page);
	}

	return pages;
}

static void 
nautilus_cvs_property_page_provider_iface_init (NautilusPropertyPageProviderIface *iface)
{
	iface->get_pages = nautilus_cvs_get_property_pages;
}

static void 
nautilus_cvs_instance_init (NautilusCvs *cvs)
{
}

static void
nautilus_cvs_class_init (NautilusCvsClass *class)
{
	parent_class = g_type_class_peek_parent (class);
}

/* Type registration.  Because this type is implemented in a module
 * that can be unloaded, we separate type registration from get_type().
 * the type_register() function will be called by the module's
 * initialization function. */
static GType cvs_type = 0;

GType
nautilus_cvs_get_type (void) 
{
	return cvs_type;
}

void
nautilus_cvs_register_type (GTypeModule *module)
{
	static const GTypeInfo info = {
		sizeof (NautilusCvsClass),
		(GBaseInitFunc) NULL,
		(GBaseFinalizeFunc) NULL,
		(GClassInitFunc) nautilus_cvs_class_init,
		NULL, 
		NULL,
		sizeof (NautilusCvs),
		0,
		(GInstanceInitFunc) nautilus_cvs_instance_init,
	};

	static const GInterfaceInfo info_provider_iface_info = {
		(GInterfaceInitFunc) nautilus_cvs_info_provider_iface_init,
		NULL,
		NULL
	};

	static const GInterfaceInfo menu_provider_iface_info = {
		(GInterfaceInitFunc) nautilus_cvs_menu_provider_iface_init,
		NULL,
		NULL
	};

	static const GInterfaceInfo property_page_provider_iface_info = {
		(GInterfaceInitFunc) nautilus_cvs_property_page_provider_iface_init,
		NULL,
		NULL
	};
	
	cvs_type = g_type_module_register_type (module,
						G_TYPE_OBJECT,
						"NautilusCvs",
						&info, 0);

	g_type_module_add_interface (module,
				     cvs_type,
				     NAUTILUS_TYPE_INFO_PROVIDER,
				     &info_provider_iface_info);

	g_type_module_add_interface (module,
				     cvs_type,
				     NAUTILUS_TYPE_MENU_PROVIDER,
				     &menu_provider_iface_info);

	g_type_module_add_interface (module,
				     cvs_type,
				     NAUTILUS_TYPE_PROPERTY_PAGE_PROVIDER,
				     &property_page_provider_iface_info);
}
